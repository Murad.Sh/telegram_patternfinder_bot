package com.shukur.patternsfinderbot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PatternsFinderBotApplication {

    public static void main(String[] args) {
        SpringApplication.run(PatternsFinderBotApplication.class, args);
    }

}
