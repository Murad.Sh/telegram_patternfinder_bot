package com.shukur.patternsfinderbot.patternsFinderBot;

import com.shukur.patternsfinderbot.util.UserPreferences;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.CallbackQuery;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.InlineKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.InlineKeyboardButton;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Component
public class PatternsFinderBot extends TelegramLongPollingBot {
    public PatternsFinderBot(@Value("${bot.token}") String botToken) {
        super(botToken);
    }

    private Map<Long, UserPreferences> userPreferencesMap = new ConcurrentHashMap<>();

    private List<String> cryptoOptions = Arrays.asList("BTC", "ETH", "XRP");
    private List<String> timeframeOptions = Arrays.asList("1m", "5m", "1h");
    private List<String> patternOptions = Arrays.asList("Doji", "Hammer", "Engulfing");


    @Override
    public String getBotUsername() {

        return "Sh_Programming_Lab_Project_6_Bot";
    }

    @Override
    public void onUpdateReceived(Update update) {
        if (update.hasMessage() && update.getMessage().hasText()) {
            String messageText = update.getMessage().getText();
            long chatId = update.getMessage().getChatId();

            if (messageText.equals("/start")) {
                showCryptoOptions(chatId);
            } else if (messageText.equals("/mychoice")) {
                UserPreferences preferences = userPreferencesMap.get(chatId);
                if (preferences != null) {
                    String currentChoice = String.format(
                            "Ваш текущий выбор:\nКриптовалюта: %s\nТаймфрейм: %s\nПаттерн: %s",
                            preferences.getSelectedCrypto(),
                            preferences.getSelectedTimeframe(),
                            preferences.getSelectedPattern()
                    );
                    sendTextMessage(chatId, currentChoice);
                } else {
                    sendTextMessage(chatId, "Вы еще не сделали выбор.");
                    }
                }
        } else if (update.hasCallbackQuery()) {
            // Обработка callback запроса от inline кнопок
            handleCallbackQuery(update.getCallbackQuery());
        }
    }

    private void handleCallbackQuery(CallbackQuery callbackQuery) {
        String callData = callbackQuery.getData();
        long chatId = callbackQuery.getMessage().getChatId();
        UserPreferences preferences = userPreferencesMap.getOrDefault(chatId, new UserPreferences());

        if (callData.startsWith("crypto_")) {
            String crypto = callData.split("_")[1];
            preferences.setSelectedCrypto(crypto);
            showTimeframeOptions(chatId); // Показать опции таймфрейма
        } else if (callData.startsWith("timeframe_")) {
            String timeframe = callData.split("_")[1];
            preferences.setSelectedTimeframe(timeframe);
            showPatternOptions(chatId); // Показать опции паттернов
        } else if (callData.startsWith("pattern_")) {
            String pattern = callData.split("_")[1];
            preferences.setSelectedPattern(pattern);

            // Здесь может быть логика, которая активирует отслеживание выбранного паттерна
            // Например, отправка сообщения с подтверждением выбора
            String confirmationMessage = String.format(
                    "Вы выбрали:\nКриптовалюта: %s\nТаймфрейм: %s\nПаттерн: %s",
                    preferences.getSelectedCrypto(),
                    preferences.getSelectedTimeframe(),
                    preferences.getSelectedPattern()
            );
            sendTextMessage(chatId, confirmationMessage);
        }

        userPreferencesMap.put(chatId, preferences); // Обновление предпочтений пользователя

    }

    private void showCryptoOptions(long chatId) {
        InlineKeyboardMarkup markupInline = new InlineKeyboardMarkup();
        List<List<InlineKeyboardButton>> rowsInline = new ArrayList<>();

        for (String crypto : cryptoOptions) {
            InlineKeyboardButton button = new InlineKeyboardButton();
            button.setText(crypto);
            button.setCallbackData("crypto_" + crypto);
            List<InlineKeyboardButton> rowInline = new ArrayList<>();
            rowInline.add(button);
            rowsInline.add(rowInline);
        }


        markupInline.setKeyboard(rowsInline);
        sendInlineKeyboard(chatId, "Выберите криптовалюту:", markupInline);
    }

    private void showTimeframeOptions(long chatId) {
        InlineKeyboardMarkup markupInline = new InlineKeyboardMarkup();
        List<List<InlineKeyboardButton>> rowsInline = new ArrayList<>();

        for (String timeframe : timeframeOptions) {
            InlineKeyboardButton button = new InlineKeyboardButton();
            button.setText(timeframe);
            button.setCallbackData("timeframe_" + timeframe);
            List<InlineKeyboardButton> rowInline = new ArrayList<>();
            rowInline.add(button);
            rowsInline.add(rowInline);
        }

        markupInline.setKeyboard(rowsInline);
        sendInlineKeyboard(chatId, "Выберите таймфрейм:", markupInline);
    }

    private void showPatternOptions(long chatId) {
        InlineKeyboardMarkup markupInline = new InlineKeyboardMarkup();
        List<List<InlineKeyboardButton>> rowsInline = new ArrayList<>();

        for (String pattern : patternOptions) {
            InlineKeyboardButton button = new InlineKeyboardButton();
            button.setText(pattern);
            button.setCallbackData("pattern_" + pattern);
            List<InlineKeyboardButton> rowInline = new ArrayList<>();
            rowInline.add(button);
            rowsInline.add(rowInline);
        }

        markupInline.setKeyboard(rowsInline);
        sendInlineKeyboard(chatId, "Выберите паттерн:", markupInline);
    }

    private void sendInlineKeyboard(long chatId, String text, InlineKeyboardMarkup markupInline) {
        SendMessage message = new SendMessage();
        message.setChatId(chatId);
        message.setText(text);
        message.setReplyMarkup(markupInline);
        try {
            execute(message);
        } catch (TelegramApiException e) {
            e.printStackTrace();
        }
    }

    private void sendTextMessage(long chatId, String text) {
        SendMessage message = new SendMessage();
        message.setChatId(chatId);
        message.setText(text);
        try {
            execute(message);
        } catch (TelegramApiException e) {
            e.printStackTrace();
        }
    }
}
