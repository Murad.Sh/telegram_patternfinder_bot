package com.shukur.patternsfinderbot.util;

import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;

@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class UserPreferences {
    String selectedCrypto;
    String selectedTimeframe;
    String selectedPattern;
}
